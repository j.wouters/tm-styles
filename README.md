# TeXmacs style packages

To use these style packages, put them in the `packages` subdirectory TeXmacs home path. In Windows go to `C:\Users\....\AppData\Roaming\TeXmacs\packages` (where you need to replace the dots by your username), in MacOS and Gnu\Linux go to `~\.TeXmacs\packages`.

Style packages can also be put in the same directory as your `.tm` document (although not on Windows currently, see bug [#61563](https://savannah.gnu.org/bugs/?61563)).

When exporting to HTML, the CSS file in this repository needs to be in the same directory as the HTML file.
