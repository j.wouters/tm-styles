<TeXmacs|2.1>

<style|source>

<\body>
  <\active*>
    <\src-title>
      <src-package|dyslexia-friendly|1.0>

      <src-purpose|A style package to make documents more dyslexia friendly>
    </src-title>
  </active*>

  <assign|font|Fira>

  <\active*>
    <\src-comment>
      Style parameters.
    </src-comment>
  </active*>

  <assign|bg-color|#fffff0>

  <assign|font-base-size|14>

  <assign|full-screen-mode|false>

  <assign|par-sep|1fn>

  <assign|page-width-margin|true>

  <assign|par-width|60ex>

  <assign|par-width|<plus|52ex|2em|2yfrac>>

  <assign|html-extra-css|<tuple|lecture-notes.css|https://code.cdn.mozilla.net/fonts/fira.css>>
</body>

<initial|<\collection>
</collection>>